# 🕒 Clock

## Project Description

Create a program to display a working clock.

## Conditions of Satisfaction (CoS)

> **As a** Full Stack Engineer candidate<br />
> **I want to** create a clock<br />
> **So that** my skills can be validated

1. ✅ Check your code into a github repo and share the link with us.
   1. ✅ Please keep in mind the Skills and Qualifications listed in the job posting.
1. ✅ Must be able to run the code locally, preferably in a browser.
1. ✅ This is a part of your portfolio. Show us something cool and your awesome coding skills. What makes you stand out?

## Technical Notes

### Git Repository

The Git repository for this project can be accessed on [GitLab](https://gitlab.com/mkuehn/anaplan-clock).

### Hosting

A live version of this project is hosted on [GitLab Pages](https://mkuehn.gitlab.io/anaplan-clock/).

You can download a version to run locally, if desired, from the GitLab page above. Extract the downloaded archive and open `/public/index.html` in a browser.

### Branch Management & Deployment

This project uses the GitFlow branching model. All work is done on the `develop` branch and merged to `master` when a feature is completed.

Merges to `master` are automatically deployed to production via [GitLab Pipelines](https://docs.gitlab.com/ee/ci/pipelines.html) per the configuration in `.gitlab-ci.yml`.

## Documentation

This program uses your device's system clock to render a large digital clock in the browser, reminiscent of the ones found on your alarm clock, stove, or VCR. The hours and minutes separator pulses at 1Hz when the clock is running so that you know the clock is working.

An indicator next to the `PM` text will illuminate during the PM hours when the clock is in 12-hour mode. The `PM` text and indicator are not shown in 24-hour mode, as they are not relevant in that mode.

The clock layout uses responsive design, so it will automatically resize to best fit the device you're viewing it on. The extended time zone information is always hidden on devices with very small screens to improve legibility.

The display is synchronized with your system clock once per second, and is updated only as needed.

### Configuration

Configuration options are set by passing an object when you call `createClock()` to create a clock.

The configuration object can be modified in `index.html` when running the program locally in your browser. Reload the page to see your changes.

#### `mode`

**Default:** 12

Determines what type of clock is shown. Provide a value of `12` for 12-hour mode or `24` for 24-hour mode; any other value is ignored.

#### `showExtendedInfo`

**Default:** true

Determines whether the additional time zone and location information are shown below the clock. Provide a value of `true` to show the additional time zone and location information, or `false` to hide the information.

### Methods

#### `setMode(mode)`

Sets the 12- or 24-mode of the clock after it has started. Does not affect a stopped clock until the clock is started again.

See the Configuration `mode` section for more information on `mode` values.

#### `start()`

Starts a stopped clock. The clock updates to the current time and the separator begins to pulse again to show that the clock is running.

#### `stop()`

Stops the currently running clock. The clock is no longer updated, and the separator is frozen to indicate the stopped state.