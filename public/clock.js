function createClock (userConfig) {
	const
		CLOCK_MIL = 24,
		CLOCK_REG = 12,

		UPDATE_MS = 1000;

	let timer;

	// Provide default clock configuration.
	let config = {
		mode: CLOCK_REG,
		showExtendedInfo: true
	};

	setConfiguration(userConfig);

	// One-time DOM lookups for elements that will need to be referenced later.
	let
		elApp = document.getElementById('app'),
		elHour = document.getElementById('hour'),
		elMinute = document.getElementById('minute'),
		elSeparator = document.getElementById('sep'),
		elAMPM = document.getElementById('ampm'),
		elTZ = document.getElementById('tz'),
		elPMIndicator = document.getElementsByClassName('pm')[0];

	return {
		/**
		 * Starts clock execution.
		 */
		start () {
			if (!timer) {
				oneTimeSetup();

				timer = setInterval(updateClock, UPDATE_MS);

				elSeparator.classList.add('pulse');
			} else {
				console.warn('This clock has already been started.');
			}
		},

		/**
		 * Stops the currently executing clock.
		 */
		stop () {
			if (timer) {
				elSeparator.classList.remove('pulse');
				timer = clearInterval(timer);
			} else {
				console.warn('This clock has already been stopped.');
			}
		},

		/**
		 * Sets a new mode on the clock, if mode is valid.
		 * @param {Number} mode A valid mode value for the clock.
		 */
		setMode (mode) {
			if (validateMode(mode)) {
				config.mode = mode;
			}
		}
	};

	/**
	 * Creates the time zone information text.
	 * @param {Object} timeFrame A point in time.
	 */
	function getTimeZoneText (timeFrame) {
		let out = '';

		try {
			out = timeFrame
					.now
					.toString()
					.split('(')[1]
					.replace(')', '');

			// Don't add additional time zone information if it can't be determined.
			if (Intl.DateTimeFormat().resolvedOptions().timeZone.length) {
				out += '<br /><small>' + Intl.DateTimeFormat().resolvedOptions().timeZone + '</small>';
			}
		} catch (e) {
			console.warn('Error creating TZ text from the timeFrame provided.');
		}

		return out;
	}

	/**
	 * Overrides default clock configuration with optional user configuration
	 * values.
	 * @param {Object=} userConfig An object of clock properties to be set.
	 */
	function setConfiguration (userConfig) {
		if (userConfig) {
			// *** Mode
			if (userConfig.hasOwnProperty('mode') && validateMode(userConfig.mode)) {
				config.mode = userConfig.mode;
			}

			// *** Extended Information
			if (userConfig.hasOwnProperty('showExtendedInfo') && typeof userConfig.showExtendedInfo === 'boolean') {
				config.showExtendedInfo = userConfig.showExtendedInfo
			}
		}
	}

	/**
	 * Validates the specified mode.
	 * @param {Number} mode A numeric mode value for the clock.
	 * @returns {Boolean} Whether mode is valid or not.
	 */
	function validateMode (mode) {
		return mode === CLOCK_MIL || mode === CLOCK_REG;
	}

	/**
	 * Executed once when the clock starts to configure the display.
	 */
	function oneTimeSetup () {
		setTimeZoneDisplay();

		// The AM/PM indicator might not always be visible, depending on configuration.
		setAMPMIndicatorVisibility();

		// Call updateClock() to show the clock immediately before the timed updates start.
		updateClock();
	}

	/**
	 * Hides the AM/PM indicator when the clock is in 24-hour mode.
	 */
	function setAMPMIndicatorVisibility () {
		if (config.mode === CLOCK_MIL) {
			elAMPM.style.display = 'none';
		} else {
			elAMPM.style.display = 'inline-block';
		}
	}

	function setTimeZoneDisplay () {
		if (config.showExtendedInfo) {
			let text = getTimeZoneText(getTimeFrame());

			elTZ.innerHTML = text;
		}
	}

	/**
	 * A method to call when the clock needs to be updated.
	 */
	function updateClock () {
		render(getTimeFrame());
	}

	/**
	 * Gets the current time and provides methods to display time parts.
	 */
	function getTimeFrame () {
		let now = new Date();

		return {
			now: now,

			h : now.getHours(),
			m : now.getMinutes(),
			s : now.getSeconds(),

			/**
			 * Returns the hours value correctly formatted for a 12- or 24-hour
			 * clock.
			 * @returns {Number} The clock's formatted hour value.
			 */
			getFormattedHours : function () {
				if (config.mode === CLOCK_MIL) {
					return this.h;
				} else {
					if (this.h > CLOCK_REG) {
						return this.h - CLOCK_REG;
					} else if (this.h === 0) {
						return CLOCK_REG;
					}

					return this.h;
				}
			},

			getHours : function () {
				return this.h;
			},
			getMinutes : function () {
				return this._padZeroLeft(this.m);
			},
			getSeconds : function () {
				return this._padZeroLeft(this.s);
			},

			/**
			 * Ensures some numeric value has a 0 in the tens place when value
			 * is less than 10.
			 * @param {Number} value A number to be formatted.
			 * @returns {String} A string representing the left-zero-padded number.
			 */
			_padZeroLeft : function (value) {
				return value.toString().padStart(2, '0');
			}
		};
	}

	/**
	 * Renders a specific time to the screen.
	 * @param {Object} timeFrame A point in time.
	 */
	function render (timeFrame) {
		// Set AM/PM visibility, as the configuration may have changed.
		setAMPMIndicatorVisibility();

		/* Hours and minutes are only updated when they change, not with every
		call to render(). */
		if (parseInt(elHour.innerHTML) !== parseInt(timeFrame.getFormattedHours())) {
			elHour.innerHTML = timeFrame.getFormattedHours();
		}

		if (parseInt(elMinute.innerHTML) !== parseInt(timeFrame.getMinutes())) {
			elMinute.innerHTML = timeFrame.getMinutes();
		}

		// The "PM" indicator gets turned off in the AM, and illuminated in the PM.
		if (timeFrame.getHours() < CLOCK_REG) {
			elPMIndicator.classList.add('am');
		} else {
			elPMIndicator.classList.remove('am');
		}
	}
};